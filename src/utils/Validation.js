const roomValidation = (values) => {
  const errors = {};

  if (!values.description) {
    errors.description = 'Please enter an email';
  }
  if (!values.name) {
    errors.name = 'Please enter the room type';
  } else if (values.name.length < 3) {
    errors.name = 'Must be at least 3 characters';
  }
  if (!values.rate) {
    errors.rate = 'Please enter a rate';
  } else if (values.rate <= 0) {
    errors.rate = 'Must be number greater than zero';
  }

  return errors;
};

const reservationValidation = (values) => {
  const errors = {};
  const regexEmail = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
  const regexcheckInDate = /^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d$/;
  if (!values.numberOfNights) {
    errors.numberOfNights = 'Please enter the number of nights';
  } else if (values.numberOfNights < 1) {
    errors.numberOfNights = 'Must be number greater than 0';
  }
  if (!values.guestEmail) {
    errors.guestEmail = 'Please enter an email';
  } else if (!regexEmail.test(values.guestEmail)) {
    errors.guestEmail = 'Must be a valid email';
  }
  if (!values.checkInDate) {
    errors.checkInDate = 'Please enter a check in date';
  } else if (!regexcheckInDate.test(values.checkInDate)) {
    errors.checkInDate = 'Date must be mm-dd-yyyy';
  }

  if (values.roomTypeId === 0) {
    errors.roomTypeId = 'Must select a room type';
  }
  return errors;
};

const errorStyle = {
  fontFamily: 'Roboto Mono, monospace',
  fontWeight: 'bold',
  fontStyle: 'italic',
  color: 'red',
  padding: '5px',
  textAlign: 'left'
};

export { roomValidation, errorStyle, reservationValidation };
