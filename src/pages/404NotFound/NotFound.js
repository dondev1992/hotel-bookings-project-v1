import React from 'react';

function NotFound() {
  const notFoundStyling = {
    display: 'flex',
    flexDirection: 'column',
    fontSize: '2rem',
    alignItems: 'center',
    justifyContent: 'center',
    margin: '5em'
  };

  return (
    <div>
      <h1 style={notFoundStyling}>404 Not Found</h1>
    </div>

  );
}

export default NotFound;
