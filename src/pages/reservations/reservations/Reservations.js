import React, { useState, useEffect } from 'react';
import { useNavigate, useOutletContext } from 'react-router-dom';
import classes from './Reservations.module.css';

function Reservations() {
  const [reservations, reservationsSet] = useState([]);
  const [roomTypes, roomTypesSet] = useState([]);

  const { authToken } = useOutletContext();

  const navigate = useNavigate();

  useEffect(() => {
    const initHeader = {
      method: 'GET',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${authToken}`
      }
    };
    const getReservations = async () => {
      try {
        const response = await fetch(
          'http://localhost:8080/reservations',
          initHeader
        );
        const response2 = await fetch(
          'http://localhost:8080/room-types',
          initHeader
        );
        const rooms = await response2.json();
        roomTypesSet(rooms);
        const data = await response.json();
        reservationsSet(data);
      } catch (error) {
        if (!error.response) {
          console.log('No server response');
        } else {
          console.log(error.response.statusCode);
        }
      }
    };
    getReservations();
  }, [authToken]);

  const deleteReservation = async (id) => {
    const tempReservations = [...reservations];
    reservationsSet(tempReservations.filter((reservation) => reservation.id !== id));

    await fetch(
      `http://localhost:8080/reservations/${id}`,
      {
        method: 'DELETE',
        mode: 'cors',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${authToken}`
        }
      }
    );
  };

  return (
    <div className={classes.main_container}>
      <h1>Reservations</h1>
      <div>
        <button className={classes.create_button} type="button" onClick={() => navigate('/reservations/create', { state: { authToken, roomTypes } })}>CREATE</button>
      </div>
      {reservations?.map((reservation) => (
        <div key={reservation.id} className={classes.container}>
          <div className={classes.inner_container}>
            <div className={classes.field_container}>
              <p style={{ fontWeight: 'bold' }}>
                Guest Email:
              </p>
              <p>
                {reservation.guestEmail}
              </p>
            </div>
            <div className={classes.field_container}>
              <p style={{ fontWeight: 'bold' }}>
                Room Type:
              </p>
              <p>
                {
                  roomTypes !== undefined ? roomTypes.find((roomType) => roomType.id === reservation.roomTypeId).name : ''
                }
              </p>
            </div>
            <div className={classes.field_container}>
              <p style={{ fontWeight: 'bold' }}>
                Check in date:
              </p>
              <p>
                {reservation.checkInDate}
              </p>
            </div>
            <div className={classes.field_container}>
              <p style={{ fontWeight: 'bold' }}>
                Number of nights:
              </p>
              <p>
                {reservation.numberOfNights}
              </p>
            </div>
            <div className={classes.field_container}>
              <p style={{ fontWeight: 'bold' }}>
                Cost of Stay:
              </p>
              <p>
                $
                {(
                  reservation.numberOfNights
                  * roomTypes.find((roomType) => roomType.id === reservation.roomTypeId)
                    .rate).toFixed(2)}
              </p>
            </div>
          </div>
          <div className={classes.button_container}>
            <button className={classes.buttons} type="button" onClick={() => navigate(`/reservations/edit/${reservation.id}`, { state: { reservation, roomTypes } })}>EDIT</button>
            <button className={classes.buttons} type="button" onClick={() => { deleteReservation(reservation.id); }}>DELETE</button>
          </div>
        </div>
      ))}
    </div>
  );
}

export default Reservations;
