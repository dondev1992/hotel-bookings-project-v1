import React, { useState } from 'react';
import { useNavigate, useLocation, useOutletContext } from 'react-router-dom';
import ReservationForm from '../../../components/reservationForm/ReservationForm';
import { reservationValidation } from '../../../utils/Validation';
import Modal from '../../../components/modal/Modal';
import classes from './EditReservation.module.css';

function EditReservation() {
  const { authToken } = useOutletContext();
  const location = useLocation();

  const [formErrors, formErrorsSet] = useState({});
  const [showModal, showModalSet] = useState(false);
  const [message, messageSet] = useState('');
  const [form, formSet] = useState({
    id: location?.state?.reservation.id,
    user: location?.state?.reservation.user,
    guestEmail: location?.state?.reservation.guestEmail,
    roomTypeId: location?.state?.reservation.roomTypeId,
    checkInDate: location?.state?.reservation.checkInDate,
    numberOfNights: location?.state?.reservation.numberOfNights
  });

  const navigate = useNavigate();

  const handleSubmit = async (event) => {
    event.preventDefault();
    const validate = reservationValidation(form);
    if (Object.keys(validate).length > 0) {
      formErrorsSet(validate);
    } else {
      try {
        const response = await fetch(
          `http://localhost:8080/reservations/${location.state.reservation.id}`,
          {
            method: 'PUT',
            mode: 'cors',
            headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${authToken}` },
            body: JSON.stringify(form)
          }
        );
        if (!response.ok) {
          throw new Error();
        }
        await response.json();
        navigate(-1);
      } catch (error) {
        if (!error.response) {
          messageSet('Opps something went wrong');
          showModalSet(true);
        }
      }
    }
  };

  const handleChange = (event) => {
    const { name } = event.target;
    const { value } = event.target;
    formSet((prev) => ({ ...prev, [name]: value }));
  };

  return (
    <div className={classes.container}>
      <Modal message={message} openModal={showModal} closeModal={() => showModalSet()} />
      <ReservationForm
        handleChange={handleChange}
        handleSubmit={handleSubmit}
        form={form}
        buttonLabel="Update"
        title="Edit Reservation"
        formErrors={formErrors}
        roomTypes={location.state.roomTypes}
      />
    </div>
  );
}

export default EditReservation;
