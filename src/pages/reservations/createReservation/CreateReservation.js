import React, { useState } from 'react';
import { useOutletContext, useNavigate, useLocation } from 'react-router-dom';
import ReservationForm from '../../../components/reservationForm/ReservationForm';
import Modal from '../../../components/modal/Modal';
import { reservationValidation } from '../../../utils/Validation';
import classes from './CreateReservation.module.css';

function CreateReservation() {
  const { userEmail } = useOutletContext();
  const { authToken } = useOutletContext();
  const [formErrors, formErrorsSet] = useState({});
  const [showModal, showModalSet] = useState(false);
  const [message, messageSet] = useState('');
  const [form, formSet] = useState({
    user: userEmail,
    guestEmail: '',
    roomTypeId: 0,
    checkInDate: '',
    numberOfNights: ''
  });

  const navigate = useNavigate();
  const location = useLocation();

  const handleSubmit = async (event) => {
    event.preventDefault();
    const validate = reservationValidation(form);
    if (Object.keys(validate).length > 0) {
      formErrorsSet(validate);
    } else {
      try {
        const response = await fetch(
          'http://localhost:8080/reservations',
          {
            method: 'POST',
            mode: 'cors',
            headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${authToken}` },
            body: JSON.stringify(form)
          }
        );
        if (!response.ok) {
          throw new Error();
        }
        await response.json();
        navigate(-1);
      } catch (error) {
        if (!error.response) {
          messageSet('Opps something went wrong');
          showModalSet(true);
        }
      }
    }
  };

  const handleChange = (event) => {
    const { name } = event.target;
    const { value } = event.target;
    formSet((prev) => ({ ...prev, [name]: value }));
  };

  return (
    <div className={classes.container}>
      <Modal message={message} openModal={showModal} closeModal={() => showModalSet()} />
      <ReservationForm
        handleChange={handleChange}
        handleSubmit={handleSubmit}
        title="Create Reservation"
        form={form}
        formErrors={formErrors}
        buttonLabel="Create"
        roomTypes={location.state.roomTypes}
      />
    </div>
  );
}

export default CreateReservation;
