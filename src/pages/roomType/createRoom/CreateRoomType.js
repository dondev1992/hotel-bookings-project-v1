import React, { useState } from 'react';
import { useOutletContext, useNavigate } from 'react-router-dom';
import RoomForm from '../../../components/roomForm/RoomForm';
import Modal from '../../../components/modal/Modal';
import { roomValidation } from '../../../utils/Validation';
import Loader from '../../../components/loader/Loader';
import classes from './CreateRoomType.module.css';

function CreateRoomType() {
  const { authToken } = useOutletContext();
  const [formErrors, formErrorsSet] = useState({});
  const [showModal, showModalSet] = useState(false);
  const [message, messageSet] = useState('');
  const [loading, loadingSet] = useState(false);
  const [form, formSet] = useState({
    name: '',
    description: '',
    rate: null,
    active: false
  });

  const navigate = useNavigate();

  const handleSubmit = async (event) => {
    loadingSet(true);
    event.preventDefault();
    const validate = roomValidation(form);
    if (Object.keys(validate).length > 0) {
      formErrorsSet(validate);
      loadingSet(false);
    } else {
      try {
        const response = await fetch(
          'http://localhost:8080/room-types',
          {
            method: 'POST',
            mode: 'cors',
            headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${authToken}` },
            body: JSON.stringify(form)
          }
        );
        if (!response.ok) {
          throw new Error();
        }
        await response.json();
        loadingSet(false);
        navigate(-1);
      } catch (error) {
        if (!error.response) {
          loadingSet(false);
          messageSet('Opps something went wrong');
          showModalSet(true);
        }
      }
    }
  };

  const handleChange = (event) => {
    const { name } = event.target;
    const { value } = event.target;
    formSet((prev) => ({ ...prev, [name]: value, active: !prev.active }));
  };

  return (
    <div className={classes.container}>
      {loading ? <Loader />
        : (
          <div>
            <Modal message={message} openModal={showModal} closeModal={() => showModalSet()} />
            <RoomForm
              handleChange={handleChange}
              handleSubmit={handleSubmit}
              title="Create Room"
              form={form}
              formErrors={formErrors}
              buttonLabel="Create"
              dropDown={false}
            />
          </div>
        )}
    </div>
  );
}

export default CreateRoomType;
