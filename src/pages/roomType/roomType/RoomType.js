import React, { useState, useEffect } from 'react';
import { useOutletContext, useNavigate } from 'react-router-dom';
import Loader from '../../../components/loader/Loader';
import Modal from '../../../components/modal/Modal';
import classes from './RoomType.module.css';

function RoomType() {
  const [rooms, roomsSet] = useState([]);
  const [showModal, showModalSet] = useState(false);
  const [loading, loadingSet] = useState(false);
  const [message, messageSet] = useState('');

  const { authToken } = useOutletContext();
  const navigate = useNavigate();

  useEffect(() => {
    const getAllRooms = async () => {
      try {
        const response = await fetch(
          'http://localhost:8080/room-types',
          {
            method: 'GET',
            mode: 'cors',
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${authToken}`
            }
          }
        );
        const data = await response.json();
        roomsSet(data);
      } catch (error) {
        if (!error.response) {
          loadingSet(false);
          messageSet('Invalid email or password');
          showModalSet(true);
        } else if (error.response) {
          loadingSet(false);
          messageSet('Opps something went wrong');
          showModalSet(true);
        }
      }
    };
    getAllRooms();
  }, [authToken]);

  const getRoomStatus = (status) => {
    if (status) {
      return 'Active';
    }
    return 'Inactive';
  };

  return (
    <div>
      {loading ? <Loader />
        : (
          <>
            <h1>Rooms</h1>
            <Modal message={message} openModal={showModal} closeModal={() => showModalSet()} />
            <div>
              <button className={classes.buttons} type="button" onClick={() => navigate('/room-types/create', { state: { authToken } })}>CREATE</button>
            </div>
            {rooms?.map((room) => (
              <div key={room.id} className={classes.container}>
                <div className={classes.inner_container}>
                  <div className={classes.field_container}>
                    <p style={{ fontWeight: 'bold' }}>
                      Room Type:
                    </p>
                    <p>
                      {room.name}
                    </p>
                  </div>
                  <div className={classes.field_container}>
                    <p style={{ fontWeight: 'bold' }}>
                      Description:
                    </p>
                    <p>
                      {room.description}
                    </p>
                  </div>
                  <div className={classes.field_container}>
                    <p style={{ fontWeight: 'bold' }}>
                      Rate:
                    </p>
                    <p>
                      {room.rate}
                    </p>
                  </div>
                  <div className={classes.field_container}>
                    <p style={{ fontWeight: 'bold' }}>
                      Active Status:
                    </p>
                    <p>
                      {getRoomStatus(room.active)}
                    </p>
                  </div>
                </div>
                <div className={classes.button_container}>
                  <button className={classes.buttons} type="button" onClick={() => navigate(`/room-types/edit/${room.id}`, { state: { room } })}>Edit</button>
                </div>
              </div>
            ))}
          </>
        )}
    </div>
  );
}

export default RoomType;
