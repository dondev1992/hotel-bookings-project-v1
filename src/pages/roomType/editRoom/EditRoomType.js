import React, { useState } from 'react';
import { useNavigate, useLocation, useOutletContext } from 'react-router-dom';
import RoomForm from '../../../components/roomForm/RoomForm';
import { roomValidation } from '../../../utils/Validation';
import Modal from '../../../components/modal/Modal';
import classes from './EditRoomType.module.css';

function EditRoomType() {
  const { authToken } = useOutletContext();
  const location = useLocation();
  const [formErrors, formErrorsSet] = useState({});
  const [showModal, showModalSet] = useState(false);
  const [message, messageSet] = useState('');
  const [form, formSet] = useState({
    id: location.state.room.id,
    user: location.state.room.user,
    name: location.state.room.name,
    description: location.state.room.description,
    rate: location.state.room.rate,
    active: location.state.room.active
  });

  const navigate = useNavigate();

  const handleSubmit = async (event) => {
    event.preventDefault();
    const validate = roomValidation(form);
    if (Object.keys(validate).length > 0) {
      formErrorsSet(validate);
    } else {
      try {
        const response = await fetch(
          `http://localhost:8080/room-types/${location.state.room.id}`,
          {
            method: 'PUT',
            mode: 'cors',
            headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${authToken}` },
            body: JSON.stringify(form)
          }
        );
        if (!response.ok) {
          throw new Error();
        }
        await response.json();
        navigate(-1);
      } catch (error) {
        if (!error.response) {
          messageSet('Opps something went wrong');
          showModalSet(true);
        }
      }
    }
  };

  const handleChange = (event) => {
    const { name } = event.target;
    const { value } = event.target;
    formSet((prev) => ({ ...prev, [name]: value, active: !prev.active }));
  };

  return (
    <div className={classes.container}>
      <Modal message={message} openModal={showModal} closeModal={() => showModalSet()} />
      <RoomForm
        handleChange={handleChange}
        handleSubmit={handleSubmit}
        form={form}
        buttonLabel="Update"
        title="Edit Room Type"
        formErrors={formErrors}
        roomTypes={location.state.roomTypes}
      />
    </div>
  );
}

export default EditRoomType;
