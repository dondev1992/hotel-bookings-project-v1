/* eslint-disable import/no-extraneous-dependencies */
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import SignInForm from '../../components/signInForm/SignInForm';
import Loader from '../../components/loader/Loader';
import classes from './SignIn.module.css';
import Modal from '../../components/modal/Modal';

function SignIn({ getAuth }) {
  const [form, formSet] = useState({
    email: '',
    password: ''
  });

  const [showModal, showModalSet] = useState(false);

  const [loading, loadingSet] = useState(false);
  const [message, messageSet] = useState('');

  const navigate = useNavigate();

  const handleSubmit = async (event) => {
    loadingSet(true);
    event.preventDefault();
    try {
      const response = await fetch(
        'http://localhost:8080/login',
        {
          method: 'POST',
          mode: 'cors',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify(form)
        }
      );
      if (!response.ok) {
        throw new Error();
      }
      const data = await response.json();
      sessionStorage.setItem('token', data.token);
      getAuth(data.token);
      loadingSet(false);
      navigate('/reservations');
    } catch (error) {
      if (!error.response) {
        loadingSet(false);
        messageSet('Invalid email or password');
        showModalSet(true);
      } else if (error.response) {
        loadingSet(false);
        messageSet('Opps something went wrong');
        showModalSet(true);
      }
    }
  };

  const handleChange = (event) => {
    const { name } = event.target;
    const { value } = event.target;
    formSet((prev) => ({ ...prev, [name]: value }));
  };

  return (
    <section>
      {loading ? <Loader />
        : (
          <div className={classes.container}>
            <Modal message={message} openModal={showModal} closeModal={() => showModalSet()} />
            <SignInForm
              handleSubmit={handleSubmit}
              handleChange={handleChange}
            />
          </div>
        )}
    </section>
  );
}

export default SignIn;
