import React from 'react';
import classes from './Modal.module.css';

function Modal({ closeModal, openModal, message }) {
  if (!openModal) {
    return null;
  }

  return (
    <div className={classes.background}>
      <div className={classes.modal}>
        <h3>{message}</h3>
        <button type="button" className={classes.button} onClick={closeModal}>Close</button>
      </div>
    </div>
  );
}

export default Modal;
