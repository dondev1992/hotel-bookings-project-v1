import React from 'react';
import { errorStyle } from '../../utils/Validation';
import classes from './ReservationForm.module.css';

function ReservationForm({
  handleSubmit, handleChange, form, buttonLabel, title, formErrors, roomTypes
}) {
  return (
    <form className={classes.container} onSubmit={handleSubmit} noValidate>
      <h1 className={classes.title}>{title}</h1>
      <div className={classes.field}>
        <label htmlFor="guestEmail">
          Email:
          <input
            type="email"
            onChange={handleChange}
            name="guestEmail"
            defaultValue={form.guestEmail}
            style={{ border: formErrors.guestEmail ? '3px solid red' : '2px solid black' }}
          />
        </label>
        <p style={errorStyle}>{formErrors.guestEmail}</p>
      </div>
      <div className={classes.field}>
        <label htmlFor="checkInDate">
          Check-in Date:
          <input type="text" onChange={handleChange} name="checkInDate" defaultValue={form.checkInDate} style={{ border: formErrors.checkInDate ? '3px solid red' : '2px solid black' }} />
        </label>
        <p style={errorStyle}>{formErrors.checkInDate}</p>
      </div>
      <div className={classes.field}>
        <label htmlFor="numberOfNights">
          Number of Nights:
          <input type="number" onChange={handleChange} name="numberOfNights" defaultValue={form.numberOfNights} style={{ border: formErrors.numberOfNights ? '3px solid red' : '2px solid black' }} />
        </label>
        <p style={errorStyle}>{formErrors.numberOfNights}</p>
      </div>
      <div className={classes.dropdown_container}>
        <label htmlFor="roomTypeId">
          Room Type:
          <select type="select" name="roomTypeId" id="roomTypeId" onChange={handleChange} style={{ border: formErrors.roomTypeId ? '3px solid red' : '2px solid black' }}>
            <option value={0}>Choose room type</option>
            {roomTypes?.filter((room) => room.active === true)
              .map((room) => (
                <option
                  key={room.id}
                  selected={room.id === form.roomTypeId}
                  value={room.id}
                >
                  {room.name}
                </option>
              ))}
          </select>
        </label>
        <p style={errorStyle}>{formErrors.roomTypeId}</p>
      </div>
      <div>
        <button className={classes.formButton} type="submit">{buttonLabel}</button>
      </div>
    </form>
  );
}

export default ReservationForm;
