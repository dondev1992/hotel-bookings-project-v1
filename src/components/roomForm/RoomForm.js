import React from 'react';
import { errorStyle } from '../../utils/Validation';
import classes from './RoomForm.module.css';

function RoomForm({
  handleSubmit, handleChange, form, buttonLabel, title, formErrors
}) {
  return (
    <div className={classes.container}>
      <form className={classes.form_container} onSubmit={handleSubmit} noValidate>
        <h1 className={classes.title}>{title}</h1>
        <div className={classes.field}>
          <label htmlFor="name">
            Room Type:
            <input type="text" onChange={handleChange} name="name" defaultValue={form.name} style={{ border: formErrors.name ? '3px solid red' : '2px solid black' }} />
          </label>
          <p style={errorStyle}>{formErrors.name}</p>
        </div>
        <div>
          <label className={classes.textarea_field} htmlFor="description">
            Description:
            <textarea type="textarea" rows={5} cols={40} onChange={handleChange} name="description" defaultValue={form.description} />
          </label>
        </div>
        <div className={classes.field}>
          <label htmlFor="rate">
            Rate:
            <input type="number" onChange={handleChange} name="rate" defaultValue={form.rate} style={{ border: formErrors.rate ? '3px solid red' : '2px solid black' }} />
          </label>
          <p style={errorStyle}>{formErrors.rate}</p>
        </div>
        <div className={classes.checkbox_container}>
          <label className={classes.checkbox_label} htmlFor="active">
            Active Status:
            {'  '}
            <input className={classes.checkbox} type="checkbox" onChange={handleChange} name="active" checked={form.active} />
          </label>
        </div>
        <div>
          <button className={classes.button} type="submit">{buttonLabel}</button>
        </div>
      </form>
    </div>

  );
}

export default RoomForm;
