import React from 'react';
import { NavLink, useNavigate } from 'react-router-dom';
// import jwtDecode from 'jwt-decode';
import classes from './Nav.module.css';

function Navbar({ role, loggedIn, loggedInSet }) {
  const navigate = useNavigate();

  const logOut = () => {
    sessionStorage.removeItem('token');
    sessionStorage.clear();
    loggedInSet(false);
    navigate('/');
  };

  return (
    <nav className={classes.navStyle}>
      <h2>Hotel Bookings</h2>
      <ul className={classes.nav_link_container}>
        {loggedIn && <li><NavLink to="/reservations" className={({ isActive }) => (isActive ? (classes.activeNavLink) : (classes.navLink))}>Reservations</NavLink></li>}
        {loggedIn && role === 'manager' && <li><NavLink to="room-types" className={({ isActive }) => (isActive ? (classes.activeNavLink) : (classes.navLink))}>Room Types</NavLink></li>}
        {loggedIn && <li><NavLink to="/" className={classes.navLink} onClick={logOut}>Log out</NavLink></li>}
      </ul>
    </nav>
  );
}

export default Navbar;
