import React from 'react';
import { Outlet, Navigate } from 'react-router-dom';

function UserLayout({
  role, authToken, getAuth, userEmail, loggedIn
}) {
  return (
    (loggedIn && (role === 'manager' || role === 'employee')) ? (
      <Outlet context={{
        role, authToken, getAuth, userEmail
      }}
      />
    )
      : (<Navigate to="/" />)
  );
}

export default UserLayout;
