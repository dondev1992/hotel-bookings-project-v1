import React from 'react';
import classes from './SignInForm.module.css';

function SignInForm({ handleChange, handleSubmit }) {
  return (
    <div className={classes.container}>
      <form onSubmit={handleSubmit} noValidate>
        <h1 className={classes.title}>Login</h1>
        <div className={classes.field}>
          <label htmlFor="email">
            Email:
            <input type="email" onChange={handleChange} name="email" />
          </label>
        </div>
        <div className={classes.field}>
          <label htmlFor="password">
            Password:
            <input type="password" onChange={handleChange} name="password" />
          </label>
        </div>
        <div>
          <button className={classes.formButton} type="submit">Login</button>
        </div>
      </form>
    </div>
  );
}

export default SignInForm;
