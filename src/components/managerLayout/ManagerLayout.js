import React from 'react';
import { Outlet, Navigate } from 'react-router-dom';

function ManagerLayout({ authToken, role }) {
  // const { role } = useOutletContext();
  return (
    role === 'manager' ? <Outlet context={{ authToken }} /> : <Navigate to="/" />
  );
}

export default ManagerLayout;
