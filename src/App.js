import React, { useState, useEffect } from 'react';
import { Routes, Route } from 'react-router-dom';
import './App.css';
import jwtDecode from 'jwt-decode';
import UserLayout from './components/userLayout/UserLayout';
import Navbar from './components/navbar/Nav';
import SignIn from './pages/signIn/SignIn';
import Reservations from './pages/reservations/reservations/Reservations';
import CreateReservation from './pages/reservations/createReservation/CreateReservation';
import EditReservation from './pages/reservations/editReservation/EditReservation';
import RoomTypes from './pages/roomType/roomType/RoomType';
import CreateRoomType from './pages/roomType/createRoom/CreateRoomType';
import EditRoomType from './pages/roomType/editRoom/EditRoomType';
import ManagerLayout from './components/managerLayout/ManagerLayout';
import NotFound from './pages/404NotFound/NotFound';

function App() {
  const [role, roleSet] = useState('');
  const [loggedIn, loggedInSet] = useState(false);
  const [authToken, authTokenSet] = useState('');
  const [userEmail, userEmailSet] = useState('');

  const getAuth = () => {
    if (sessionStorage.getItem('token') !== null) {
      const token = sessionStorage.getItem('token');
      const tokenToString = JSON.stringify(token);
      const decoded = jwtDecode(tokenToString);
      userEmailSet(decoded.sub);
      authTokenSet(token);
      loggedInSet(true);
      roleSet(decoded.roles);
    }
  };

  useEffect(() => {
    getAuth();
  }, []);

  return (
    <div className="App">
      <Navbar role={role} loggedIn={loggedIn} loggedInSet={loggedInSet} />
      <Routes>
        <Route path="/" element={<SignIn getAuth={getAuth} />} />
        <Route element={(
          <UserLayout
            role={role}
            authToken={authToken}
            getAuth={getAuth}
            userEmail={userEmail}
            loggedIn={loggedIn}
          />
        )}
        >
          <Route path="/reservations">
            <Route index element={<Reservations />} />
            <Route path="create" element={<CreateReservation />} />
            <Route path="edit/:id" element={<EditReservation />} />
          </Route>
          <Route element={<ManagerLayout authToken={authToken} role={role} />}>
            <Route path="/room-types">
              <Route index element={<RoomTypes />} />
              <Route path="create" element={<CreateRoomType />} />
              <Route path="edit/:id" element={<EditRoomType />} />
            </Route>
          </Route>
        </Route>
        <Route path="*" element={<NotFound />} />
      </Routes>
    </div>
  );
}

export default App;
